## Board Game Server Implementation in Java
(A Client/Server Internet Utility Tool)

---

This simple game server application is for use by BSc Computer Science students at Birmingham City University, and supports the coursework project for the module CMP5308 Advanced programming UG2.

The game server matches clients with opponents, and relays moves between them. For more information, see the protocol specification on Moodle.

